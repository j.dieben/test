<!--- Please add to title 'EPIC' (i.e. EPIC xyz) -->

### EPIC Description:
*[Please decribe the highlights of the EPIC of Feature here. What is it, what are the benefits, when is it needed, are there any dependencies]*


### EPIC Resources:

* Mockups: *[Here goes a GDrive URL to the mockup(s)]*;
* Reference sites/platforms: *[Here goes a URL to the reference site(s)]*;


### Notes

*[Some complementary notes if necessary:]*

* > Here goes a quote from an email
* Here goes whatever useful information can exist…


<!--- ## DO NOT FORGET TO LINK USER STORIES/ISSUES TO APPLICABLE EPIC!! -->