**As a** *[user concerned by the story]*

**I want** *[goal of the story]*

**so that** *[reason for the story]*



### Acceptance Criteria

1. *[If I do A.]*
1. *[B should happen.]*


[
*Also, here are a few points that need to be addressed:*

1. Constraint 1;
1. Constraint 2;
1. Constraint 3.
]



### Tasks
* [ ] Task 1
* [ ] Task 2
* [ ] Task 3
* [ ] Testing Test Environment
* [ ] Testing Staging Environment



### Resources:

* Mockups: [Here goes a URL to or mockup(s) in inVision];
* Testing URL: [Here goes a URL to the testing branch or IP];
* Staging URL: [Here goes a URL to the feature on staging];



### Notes

[Some complementary notes if necessary:]

* > Here goes a quote from an email
* Here goes whatever useful information can exist…